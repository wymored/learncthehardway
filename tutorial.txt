

Setup

Linux

On most Linux systems you just have to install a few packages. For Debian based systems, like Ubuntu you should just have to install a few things using these commands:

$ sudo apt-get install build-essential

The above is an example of a command line prompt, so to get to where you can run that, find your "Terminal" program and run it first. Then you'll get a shell prompt similar to the $ above and can type that command into it. Do not type the ``$``, just the stuff after it.

Here's how you would install the same setup on an RPM based Linux like Fedora:

$ su -c "yum groupinstall development-tools"

Once you've run that, you should be able to do the first Exercise in this book and it'll work. If not then let me know.
