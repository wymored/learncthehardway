#include <stdio.h>
#include <ctype.h>

// forward declarations
int can_print_it(char ch);
void print_letters();



void print_letters()
{
    int i = 0;

    while(i< 255 ) {

        printf("'%d' == %c \n", i, i);
        i++;
    }

    printf("\n");
}

int can_print_it(char ch)
{
    return isalpha(ch) || isblank(ch);
}


int main(int argc, char *argv[])
{
    print_letters(argc, argv);
    return 0;
}
