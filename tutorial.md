
http://c.learncodethehardway.org/

*Setup

**Linux

On most Linux systems you just have to install a few packages. For Debian based systems, like Ubuntu you should just have to install a few things using these commands:

$ sudo apt-get install build-essential

The above is an example of a command line prompt, so to get to where you can run that, find your "Terminal" program and run it first. Then you'll get a shell prompt similar to the $ above and can type that command into it. Do not type the ``$``, just the stuff after it.

Here's how you would install the same setup on an RPM based Linux like Fedora:

$ su -c "yum groupinstall development-tools"

Once you've run that, you should be able to do the first Exercise in this book and it'll work. If not then let me know.


* Compile
Here is a simple first program you can make in C:

int main(int argc, char *argv[])
{
    puts("Hello world.");

    return 0;
}

You can put this into a ex1.c then type:

$ make ex1
cc     ex1.c   -o ex1

Your computer may use a slightly different command, but the end result should be a file named ex1 that you can run.

* Makefile

CFLAGS=-Wall -g

clean:
   rm -f ex1

* Valgrind

Install valgrind and run

$ valgrind ./ex4

* pointers

variables that start with * are pointers current value of array
